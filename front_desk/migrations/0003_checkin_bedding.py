# Generated by Django 4.2.14 on 2024-07-20 12:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("front_desk", "0002_checkin_meal_vouchers"),
    ]

    operations = [
        migrations.AddField(
            model_name="checkin",
            name="bedding",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="checkin",
            name="returned_bedding",
            field=models.BooleanField(default=False),
        ),
    ]
