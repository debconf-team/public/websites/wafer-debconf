# Generated by Django 2.2.14 on 2020-07-18 14:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('register', '0009_backfill_registration_timestamps'),
    ]

    operations = [
        migrations.AddField(
            model_name='attendee',
            name='shipping_address',
            field=models.TextField(blank=True),
        ),
    ]
