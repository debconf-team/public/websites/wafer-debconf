# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-26 19:20+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: minidebconf/admin.py:9 forms.py:54
msgid "Full name"
msgstr ""

#: minidebconf/admin.py:13
msgid "Involvement"
msgstr ""

#: minidebconf/admin.py:17
msgid "Accomodation"
msgstr ""

#: minidebconf/admin.py:21 forms.py:106
msgid "Food"
msgstr ""

#: minidebconf/admin.py:25
msgid "Travel"
msgstr ""

#: minidebconf/forms.py:72
msgid "I would like to stay at the conference-arranged accommodation"
msgstr ""

#: minidebconf/forms.py:74
msgid "I would like to have the conference-arranged meals"
msgstr ""

#: minidebconf/forms.py:76
msgid "I would like to request reimbursement of my travel costs"
msgstr ""

#: minidebconf/forms.py:78
#, python-format
msgid "Estimated travel cost (upper bound, in %(currency)s)"
msgstr ""

#: minidebconf/forms.py:93
#, python-format
msgid ""
"<p><a href='%s' target='_blank'>See this page</a> for more information about "
"shirt sizes.</p>"
msgstr ""

#: minidebconf/forms.py:98
msgid "T-Shirt"
msgstr ""

#: minidebconf/forms.py:113
#, python-format
msgid ""
"<p><a href='%s' target='_blank'>See this page</a> for more information about "
"bursaries.</p>"
msgstr ""

#: minidebconf/forms.py:118
msgid "Bursaries"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:11
msgid "Home page"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:16
msgid "About"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:17
msgid "about"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:21
msgid "About the event"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:22
msgid "event"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:26
msgid "Code of Conduct"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:27
msgid "coc"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:31
msgid "Organizers"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:32
msgid "org"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:38
msgid "Contribute"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:39
msgid "contribute"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:43
msgid "Call for Proposals"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:44
msgid "cfp"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:48
msgid "Important dates"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:49
msgid "important-dates"
msgstr ""

#: minidebconf/management/commands/init_minidc_menu_pages.py:55
msgid "Schedule"
msgstr ""

#: minidebconf/models.py:14
msgid "Beginner"
msgstr ""

#: minidebconf/models.py:15
msgid "User"
msgstr ""

#: minidebconf/models.py:16
msgid "Contributor"
msgstr ""

#: minidebconf/models.py:17
msgid "Debian Maintainer (DM)"
msgstr ""

#: minidebconf/models.py:18
msgid "Debian Developer (DD)"
msgstr ""

#: minidebconf/models.py:25 models.py:43
msgid "Description"
msgstr ""

#: minidebconf/models.py:34 models.py:110
msgid "Diet"
msgstr ""

#: minidebconf/models.py:51
msgctxt "conference"
msgid "registration"
msgstr ""

#: minidebconf/models.py:52
msgctxt "conference"
msgid "registrations"
msgstr ""

#: minidebconf/models.py:71
msgid "Level of involvement with Debian"
msgstr ""

#: minidebconf/models.py:78
msgid "Gender"
msgstr ""

#: minidebconf/models.py:84
msgid "Country"
msgstr ""

#: minidebconf/models.py:89
msgid "City/State or Province"
msgstr ""

#: minidebconf/models.py:95
msgid "Shirt size"
msgstr ""

#: minidebconf/models.py:100
msgid "Conference-arranged accommodation"
msgstr ""

#: minidebconf/models.py:104
msgid "Conference-arranged food"
msgstr ""

#: minidebconf/models.py:115
msgid "Reimburse travel costs"
msgstr ""

#: minidebconf/models.py:121
msgid "Travel costs"
msgstr ""

#: minidebconf/models.py:126
msgid "Notes"
msgstr ""

#: minidebconf/models.py:132
msgid "Which days you will attend"
msgstr ""

#: minidebconf/templates/minidebconf/register.html:8
msgctxt "conference"
msgid "Update registration"
msgstr ""

#: minidebconf/templates/minidebconf/register.html:10
msgctxt "conference"
msgid "Register"
msgstr ""

#: minidebconf/templates/minidebconf/registration_confirm_delete.html:5
msgid "Cancel registration"
msgstr ""

#: minidebconf/templates/minidebconf/registration_confirm_delete.html:7
msgid ""
"Are you sure you want to cancel your registration? You can register again "
"later if you change your mind."
msgstr ""

#: minidebconf/templates/minidebconf/registration_confirm_delete.html:14
msgid "No, abort"
msgstr ""

#: minidebconf/templates/minidebconf/registration_confirm_delete.html:15
msgid "Yes, I'm sure"
msgstr ""

#: minidebconf/templates/minidebconf/registration_finished.html:5
msgid "Registration complete"
msgstr ""

#: minidebconf/templates/minidebconf/registration_finished.html:7
msgid "Your registration was completed with success."
msgstr ""

#: minidebconf/templates/minidebconf/registration_finished.html:13
msgid ""
"If you need to update any information, you can use the \"Update "
"Registration\" button on your profile page."
msgstr ""

#: minidebconf/templates/minidebconf/registration_finished.html:20
msgid "OK"
msgstr ""
